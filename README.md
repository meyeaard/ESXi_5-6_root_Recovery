# ESXi_5-6_root_Recovery

VIB package and scripts which can be used to recover the root password of any ESXi host that is managed by a vCenter or can otherwise gain access to the VMHost via PowerCLI Get-ESXCli.

## The Problem
As many I'm sure, I've inherited vSphere environments through various means and with little to no information provided during transition. In such cases there are documented procedures for recovering the root and Administrator account passwords within vCenter as long as you can reach the console of a VCSA or Windows vCenter Server, however, I have found limited options for recovering the ESXi root password outside of host profiles and when faced with hosts without shared storage and running VMs with no known owners we may not be able to power down the VMs so that we may be able to place the host into maintenance mode to enable the policy. Notwithstanding this, a faster and less impacting method for recovering the root account across multiple hosts may also at times have value.

## First, Credit where it is due.
We all at times stand on the shoulders of giants, and in this case I have to credit William Lam of VirtuallyGhetto as his article 'How to create a custom ESXi VIB to execute a script once?' was my Eureka moment and his articles 'A Docker Container for building custom ESXi VIBs' and 'Creating Custom VIBs For ESXi 5.0 & 5.1 with VIB Author Fling' were concise and helpful;. 
- https://www.virtuallyghetto.com/2015/07/how-to-creating-a-custom-esxi-vib-to-execute-a-script-once.html
- https://www.virtuallyghetto.com/2015/05/a-docker-container-for-building-custom-esxi-vibs.html
- https://www.virtuallyghetto.com/2012/09/creating-custom-vibs-for-esxi-50-51.html

## The Solution
Given William Lam's contribution above the solution is simple, at the host side VIBs are installed under the root context and thus any script that executes during a VIB installation would have local privilege to reset the root account password using the standard 'passwd' CLI utility.

Within this repository you will find an offline VIB depot file as well as a PowerCLI script. The VIB file is what does all the work (you can see the contents of the VIB under the 'RootPasswordRecovery_stage' folder in this repo); when installed the VIB install script runs passwd to change the root password. The included PowerCLI script simplifies and automates the solution by copying the VIB to a datastore accessible to the host, installing the VIB via Get-EsxCli, and finally deleting the VIB. 

It should be noted that you won't be able to apply this VIB using vSphere Update Manager because the VIB has to be built as a 'Partner' update and not 'Community' in order for it to add files to the hosts /etc./ directory, and thus is not signed. Because of this you must use an installation method that allows you ignore the certificate mismatch which I understand is not possible via VUM.

Thus far I have used this on ESXi 5.1, 5.5, 6.0, 6.5, and 6.7 hosts. I have not tested against 5.0 or 7.0 but expect it should work.

## Building
From the sample directory structure provided by William in the Docker image I deleted the extraneous files from his example.
Then modified the descriptor.xml for the name, version, vendor, summary, and ensuring that acceptance-level = partner and live-install-allowed and live-remove-allowed were both set to true. Created a simple script to be executed upon install that accepts the command-line arguments as detailed by William, but in my case using switch instead of the if block used in his example. Finally build the bundle. This was all done as directed in the 'Creating Custom VIBs For ESXi 5.0 & 5.1 with VIB Author Fling'. For your amusement or if you choose to build your own and not trust my VIB (smart), I've included a tar of my staging directory used to build the VIB here.

1. Start with an environment with the vmware vibauthor fling installed.  
(I recommend William Lam's Docker image)
2. Download my RootPasswordRecovery_stage.tgz archive and extract it somewhere convenient.
3. Check out the contents to validate any concerns.
4. Modify ./stage/payloads/payload1/etc/init.d/root-password-recovery if you wish to change the recovery root password for your package (recommended).
5. Modify ./stqage/descriptor.xml as desired - leave acceptance-level, live-install-allowed, and live-remove-allowed alone to retain functionality.
5. cd back to the parent directory.
6. `vibauthor -C -t stage -v RootPasswordRecovery.vib -O RootPasswordRecovery-offline-bundle.zip -f`  
(Note in above vibauthor command, the -f is required as we are building a 'partner' VIB without appropriate certificate.)

## Caution
This project will likely raise some eyebrows and security concerns and for just reason. Just as many other useful utilities and processes that are of value to systems administrators and engineers, in the wrong hands this could be used to quickly penetrate virtual infrastructure environments. Proper security is a multi-layered beast and you are responsible for ensuring that your systems are secured properly and for obtaining release from your management and security teams before using any method, standard or nefarious, to gain access or credentials which you have not been provided. As for my packaging and making this available I would say that just because this information may not have been so publicly available so far does not at all indicate that the knowledge is not already available or that other's with dark intentions are not already using it. There is a definite value to be had by administrators and engineers which should not be overlooked by taking silence and obfuscation as a methods for implementing 'security'.

Concerned and want to protect yourself from this type of 'attack'... well any user in your vCenter who has access to install VIBs could use this, so you'll need to restrict host administration roles. If your ESXi hosts are using AD authentication and you have other non-root admin or operations role accounts directly on the ESXi host you'll need to restrict access there as well or remove such access. Or for a failsafe option: power down all VMs, power off your hosts, get a big hammer, use it. 

## Usage
As soon as the VIB is installed you should be able to access the host as root using 'P4ssw0rd!'
Obviously after any method used, IMMEDIATELY reset the root password to something secure.

If you just want to grab and go you'll just need to snag one of the VIB files in this repository, for the examples below I'm using the offline-bundle.

1. Manually.
Use the vSphere client or other method to place the VIB on a datastore accessible to the target host.  
`(Get-ESXCli -VMHost "targethost.internal.dom").software.vib.install("/vmfs/volumes/MyDatastoreName/RootPasswordRecovery-offline-bundle.zip",$false, $false, $false, $false, $true, $null, $null, $null)`  
(It's that fourth from last argument set to $true that disables signature validation on the VIB and allows us to install this as a partner bundle)

Using the included PowerCLI script
1. Using specified datastore for VIB bundle placement  
From PowerCLI `& '.\Password-Recovery.ps1' -vCenter vcenter01.corp.lo -VMHost esx01.corp.lo -Datastore esx01-local`

2. Letting the script choose an available datastore for VIB placement  
`& '.\Password-Recovery.ps1' -vCenter vcenter01.corp.lo -VMHost esx01.corp.lo`
