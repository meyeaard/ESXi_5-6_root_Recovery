﻿Param(
    [Parameter(HelpMessage="Managing vCenter.",Mandatory=$true)]$vCenter,
    [Parameter(HelpMessage="VMHost where you need to recover root password.",Mandatory=$true)]$VMHost,
    [parameter(HelpMessage="Name of datastore to locate VIB bundle - if absent we'll just grab one.")][string]$Datastore=$null,
    [parameter(HelpMessage="Leave VIB bundle on datastore.")][bool]$Dirty=$false
)

<#

.SYNOPSIS
    Recovers root password on vCenter managed ESXi host

.DESCRIPTION
    This script will use a password recovery VIB offline-bundle to recover an ESXi host root account password.
    The contents of the VIB bundle is just a single script that changes the password to P4ssw0rd! when the VIB is installed.

.REQUIREMENTS
    1. ESXi host must be managed by a vCenter where you have Host admin rights.
    2. ESXi host must have attached datastore to place VIB bundle file.
    3. *YOU HAVE THE RIGHT TO CHANGE THE PASSWORD*

.NOTES
    Author: Aaron Meyer

.PARAMETER vCenter (Required)
    vCenter VM that manages ESXi host

.PARAMETER VMHost (Required)
    VMHost where we need to recover root password

.PARAMETER Datastore
    Datastore to copy VIB bundle to.

.PARAMTER Dirty ($false)
    Dirty mode, when $true the VIB bundle file is left on the datastore.
    Possibly usefull if recovering password on multiple hosts on a single shared datastore so you don't have to recopy it.
    Not a good idea unless you know what you're doing.


.EXAMPLES
    Using specfied datastore for VIB bundle placement
    & '.\Password-Recovery.ps1' -vCenter vcenter01.corp.lo -VMHost esx01.corp.lo -Datastore esx01-local

    Letting script choose available datastore for VIB placement
    & '.\Password-Recovery.ps1' -vCenter vcenter01.corp.lo -VMHost esx01.corp.lo
  

.REFERENCES
    VIB Creation based on information obtained here:
       Creating custom VIBs
        https://www.virtuallyghetto.com/2012/09/creating-custom-vibs-for-esxi-50-51.html
       Docker container to make VIB creation easy
        https://www.virtuallyghetto.com/2015/05/a-docker-container-for-building-custom-esxi-vibs.html
       What led me down this path, a reference to a VIB executing a script when installed
        https://www.virtuallyghetto.com/2015/07/how-to-creating-a-custom-esxi-vib-to-execute-a-script-once.html            
#>


###############
## Global Vars

# Generate random PSDrive name so we don't step on anything.
$drvName = "recoveryDS_" + (Get-Random)
$OfflineBundle="RootPasswordRecovery-offline-bundle.zip"
$VIBName="RootPasswordRecovery"


#################
## Sanity checks

# Verify $OfflineBundle exists in local directory.
if ( !(Test-Path -Path ".\$OfflineBundle") ){
    Write-Warning "Missing $OfflineBundle file in local working directory"
    return(0)
}


##########
## Script

# Are we already connected to this vCenter?
if ( $vCenter -in $global:DefaultVIServers.name){ 
    Write-Host "Already Connected to $vCenter." 
} else { 
    Try {
        Write-Host "Connecting to $vCenter"
        Connect-VIServer $vCenter -ErrorAction stop
    } Catch {
        Write-Warning "Unable to connect to vCenter $vCenter"
        return(0)
    }
}

# Get VMHost object
Try { 
    $VMHost = Get-VMHost -Server $vCenter -Name $VMHost -ErrorAction Stop
    Write-Host "Connected to $VMHost"
} Catch {
    Write-Warning ("Unable to connect to $VMHost through vCenter $vCenter")
    return 0
} 

# If no datastore is specified, just grab an available one.
if (!($Datastore)){
    Write-Host "No datastore specified, automatically choosing one"
    Write-Host "Getting list of datastores on $VMHost..."
    $Datastore = ($VMHost | Get-Datastore | ?{$_.State -eq 'Available'})[0].Name
    Write-Host "We will use datastore: $Datastore"
}
    
# Verify specified datastore is accessible by specified VMHost
if ( (($VMHost | Get-Datastore).Name).Contains($Datastore) ){
    Write-Host "Verfied $Datastore is available to $VMHost"
} else {
    Write-Warning "$Datastore is not available to $VMHost, unable to continue."
    return(0)
}

# Copy VIB to specified datastore if needed.
Write-Host "Mounting $Datastore as PSDRive $drvName`:\..."
Try {
    $dsObj = Get-Datastore -Name "$Datastore" -ErrorAction stop
    New-PSDrive -Location $dsObj -Name $drvName -PSProvider VimDatastore -Root "\" -ErrorAction Stop
} Catch {
    Write-Warning "Unable to mount $Datastore as $drvName"
    return(0)
}
if (Test-Path -Path ($drvName+":\$OfflineBundle") ){
    Write-Host "Password recovery VIB bundle is already on datastore."
} else {
    Write-Host "Copying VIB bundle to $Datastore..."
    Try {
        Copy-DatastoreItem -Item $OfflineBundle -Destination ($drvName+":\$OfflineBundle") -Confirm:$false -ErrorAction stop
    } Catch {
        Write-Warning ("Unable to copy $OfflineBundle to "+$drvName+":\$OfflineBundle")
        return(0)
    }
}

Write-Host "Obtaining ESXCli on $VMHost"
Try {
    $ESXCli = Get-EsxCli -Server $vCenter -VMHost $VMHost -ErrorAction stop
} Catch {
    Write-Warning "Unable to obtain ESXCli on $VMHost"
    return(0)
}

# ESXCli commands do not support -ErrorAction so we can't use Try/Catch - capturing return value to verify.
# Install the VIB - password will be changed when VIB is installed.
$retval = $ESXCli.software.vib.install("/vmfs/volumes/$Datastore/$OfflineBundle",$false, $false, $false, $false, $true, $null, $null, $null)
if (($retval.Message).Contains("successfully")){
    Write-Host "Password Recovery VIB successfully installed"
    Write-Host -ForegroundColor:Yellow -BackgroundColor:DarkBlue "root password should now be 'P4ssw0rd!'"
} else {
    Write-Warning "Password Recovery VIB was not installed"
    $retval
}
Write-Host "VIB package has done it's job, removing VIB..."
$retval = $ESXCli.software.vib.remove($false, $false, $false, $false, $VIBName)
if (($retval.Message).Contains("successfully")){
    Write-Host "Password Recovery VIB successfully removed"
} else {
    Write-Warning "Password Recovery VIB was not uninstalled"
    $retval
}  


# Dirty mode leaves VIB bundle file on datastore, generally a bad idea. 
# Might be usefull if recovering multiple VMHosts with a shared datastore.
if (-not $Dirty){
    Write-Host "Deleting VIB bundle file."
    Remove-Item -Path ($drvName+":\$OfflineBundle") -Confirm:$false -ErrorAction stop
} else {
    # Leaving it dirty
    Write-Host -ForegroundColor:Yellow -BackgroundColor:red "`nDirty mode, leaving $OfflineBundle on $Datastore`n"
}
Write-Host "Unmounting PSDrive $drvName to $Datastore"
Remove-PSDrive -Name $drvName

# Final detail to console
Write-Host -ForegroundColor:Yellow -BackgroundColor:DarkBlue "Please check root account access with password 'P4ssw0rd!'`n"
Write-Host -ForegroundColor:Yellow -BackgroundColor:Red "-----------------------------------`n   Change the root password NOW!   `n-----------------------------------"